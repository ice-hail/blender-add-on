### Ice-Hail Add-On for Blender 2.8+

Download the "ice-hail-addon.zip" file above.
Make sure you also have Ice-Hail installed: https://www.npmjs.com/package/ice-hail

Download the .zip file and do NOT extract it.
Within blender navigate to "Edit > Preferences > Add-ons" and click on the "Install" button to select the .zip file.
The Add-on should appear in the list now, be sure to check the checkbox to activate it!

After that, you will see a new "Havok Compound (.hksc)" entry in "File > Export".