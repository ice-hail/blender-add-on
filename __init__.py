#
# @copyright 2019 - Max Bebök
# @author Max Bebök
# @license GNU-GPLv3 - see the "LICENSE" this.file in this directory
#

bl_info = {
    "name" : "Ice-Hail",
    "author" : "Max Bebök",
    "url": "https://www.npmjs.com/package/ice-hail",
    "description" : "Exporter for Havok collision using Ice-Hail",
    "blender" : (2, 80, 0),
    "version" : (1, 0, 0),
    "location" : "File > Export > Havok Compound (.hksc)",
    "warning" : "",
    "category" : "Import-Export"
}

import bpy
import subprocess
import os
import pipes

from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator

def quote_path(path):
    if os.name == 'nt':
        # well thanks python for not providing anything useful for windows
        return '"' + path + '"'
    return pipes.quote(path)

def ShowMessageBox(title, message, icon = 'INFO'):
    def draw(self, context):
        self.layout.label(text = message)
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

def create_hksc(context, filepath, compressed):
    print("Start (S)HKSC conversion...")

    create_flags = "-y " if compressed else ""
    filepath = bpy.path.abspath(filepath)
    filepath_obj = bpy.path.abspath(filepath + ".tmp.obj")
    filepath_json = bpy.path.abspath(filepath + ".tmp.json")

    # export as normalized OBJ file
    bpy.ops.export_scene.obj(
        filepath=filepath_obj, check_existing=True, 
        axis_forward='-Z', axis_up='Y', 
        filter_glob="*.obj;*.mtl", 
        use_selection=False, use_animation=False, 
        use_mesh_modifiers=True, use_edges=False, 
        use_smooth_groups=False, use_smooth_groups_bitflags=False, 
        use_normals=False, use_uvs=False, use_materials=False, 
        use_triangles=True, use_nurbs=False, 
        use_vertex_groups=False, use_blen_objects=True, 
        group_by_object=False, group_by_material=False, 
        keep_vertex_order=False, 
        global_scale=1, path_mode='AUTO'
    )

    # call ice-hail to create the JSON and the havok file
    try:
        cmd_import = "ice-hail-import -c " + quote_path(filepath_obj) + " " + quote_path(filepath_json)
        print(subprocess.check_output(cmd_import, shell=True))

        cmd_import = "ice-hail-create " + create_flags + quote_path(filepath_json) + " " + quote_path(filepath)
        print(subprocess.check_output(cmd_import, shell=True))

    except Exception as e:
        print(e)
        ShowMessageBox("Error creating HKSC", "Make sure ice-hail is installed on yor PC with 'npm i -g ice-hail'!", "ERROR")
        return {'CANCELLED'}

    # clean up and delete temp files
    os.remove(filepath_obj)
    os.remove(filepath_json)

    return {'FINISHED'}


class ExportIceHail(Operator, ExportHelper):
    """Compound Shape export with Ice-Hail"""
    bl_idname = "export_ice_hail.hksc"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Export HKSC"

    # ExportHelper mixin class uses this
    filename_ext = ".hksc"

    filter_glob: StringProperty(
        default="*.hksc",
        options={'HIDDEN'}
    )

    def execute(self, context):
        return create_hksc(context, self.filepath, False)

class ExportIceHailYaz0(Operator, ExportHelper):
    """Compressed Compound Shape export with Ice-Hail"""
    bl_idname = "export_ice_hail.shksc"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Export SHKSC"

    # ExportHelper mixin class uses this
    filename_ext = ".shksc"

    filter_glob: StringProperty(
        default="*.shksc",
        options={'HIDDEN'}
    )

    def execute(self, context):
        return create_hksc(context, self.filepath, True)

# Add export option to the menu
def menu_func_export(self, context):
    self.layout.operator(ExportIceHail.bl_idname, text="Havok Compound (.hksc)")
    self.layout.operator(ExportIceHailYaz0.bl_idname, text="Havok Compound Yaz0 (.shksc)")

def register():
    bpy.utils.register_class(ExportIceHail)
    bpy.utils.register_class(ExportIceHailYaz0)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    bpy.utils.unregister_class(ExportIceHail)
    bpy.utils.unregister_class(ExportIceHailYaz0)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

# For testing within blender
if __name__ == "__main__":
    register()