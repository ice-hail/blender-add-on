# Zip script in new directory
mkdir ice-hail-addon
cp __init__.py ice-hail-addon/__init__.py
zip -r ice-hail-addon.zip ./ice-hail-addon/__init__.py
rm -rf ./ice-hail-addon

echo ""
echo "====== UPLOAD ======"
echo ""

# Upload file to gitlab
FILE_BASE_URL="https://www.gitlab.com/ice-hail/blender-add-on"
FILE_URL=$(curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" --form "file=@ice-hail-addon.zip" \
"https://gitlab.com/api/v4/projects/12349860/uploads" | jq -r '.url')

echo "URL: $FILE_BASE_URL$FILE_URL"

rm ice-hail-addon.zip

echo ""
echo "====== RELEASE ======"
echo ""

RELEASE_TAG=$(git describe --tags)
RELEASE_NAME=$(echo "Ice-Hail Blender Add-On - $RELEASE_TAG")
RELEASE_DESCRIPTION=$(sed 's/$/\n<br\/>/' RELEASE_README.md | tr '\n' ' ' | jq -aR . | sed 's/<br\/>/<br\/>\\n/')

RELEASE_JSON=$(echo "{\
\"tag_name\": \"${RELEASE_TAG}\", \
\"name\": \"${RELEASE_NAME}\", \
\"description\": ${RELEASE_DESCRIPTION}, \
\"assets\": {\"links\": [{\"name\": \"ice-hail-addon.zip\", \"url\": \"${FILE_BASE_URL}${FILE_URL}\"}]}}")

echo $RELEASE_JSON

# Create release in gitlab

curl -i --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" --request DELETE "https://gitlab.com/api/v4/projects/12349860/releases/${RELEASE_TAG}"
curl -i --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" --header 'Content-Type: application/json' --data "${RELEASE_JSON}" --request POST "https://gitlab.com/api/v4/projects/12349860/releases"